async function ipFinder() {
    let response = await fetch('https://api.ipify.org/?format=json')
        .then(res =>  res.json());

    let ipApiRes = await fetch(
        `http://ip-api.com/json/${response.ip}?lang=ru&fields=continent,country,regionName,city,district,query`)
        .then(res => res.json());

    let para = document.createElement('p');
    para.innerText = `Континент: ${ipApiRes.continent}
                      Страна: ${ipApiRes.country}
                      Регион: ${ipApiRes.regionName}
                      Город: ${ipApiRes.city}
                      Район: ${ipApiRes.district}`;
    document.body.append(para);
}

ipFinder();

